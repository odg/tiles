/** @file tiles.c
@author Copyright (C) 2019 Oliver Galvin <odg@riseup.net>
@brief
  This is the main source file which draws the board, creates and manages GTK interface widgets.
@section LICENSE
  This file is part of Tiles, a GTK-based crossword game.

  Tiles is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Tiles is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Tiles.  If not, see <https://www.gnu.org/licenses/>.
 */

/*Note:
#include <fontconfig/fontconfig.h>

    if(!FcConfigAppFontAddFile(FcConfigGetCurrent(), (const FcChar8*)fontfile)) {
        g_error("FcConfigAppFontAddFile failed");
        return NULL;
    }
*/

#define IMG_DIR    "images"
#define LEXICA_EXT ".list"
#define LEXICA_DIR "lexica"
#define ALPHA_EXT  ".quackle_alphabet"
#define ALPHA_DIR  "alphabets"

#define HANDY_USE_UNSTABLE_API
#define GDK_DISABLE_DEPRECATED
#define GTK_DISABLE_DEPRECATED

#include "config.h"
#include "modes.h"
#include <stdlib.h>
#include <handy.h> // HdyDialog, HdyComboRow, HdyLeaflet
#include <librsvg/rsvg.h>

/** Record of all configuration data */
typedef struct config_t
{
	/* Values which determine the dimensions of the board in the main window */
	gint       square_num;  ///< Number of squares in a row/column
	gint       border_size; ///< Thickness of border within and around the board, in pixels
	gint       board_width; ///< Overall width of board, in pixels
	gint       tile_width;  ///< Width of each tile, in pixels
	double     margin;      ///< Margin around the board, as a proportion of window size
	/* Colours used on the board and tiles */
	GdkRGBA font_colour, border_colour, *sq_colour;

	gchar     *lang;        ///< Current language used for word list
	gchar     *lang_var;    ///< Current language variant, if applicable
	gchar     *mode;        ///< Current mode, i.e. rule set
} config_t;

typedef struct letter_t
{
	gchar     *label;
	gint64     freq;
	gint64     score;
} letter_t;

typedef struct lang_t
{
	gchar    **words;
	gint       w_num;
	letter_t  *letters;
	gint       l_num;
} lang_t;

typedef struct root_t
{
	GtkWidget *window;
	square_t **board;
	config_t  *config;
	lang_t    *lang;
} root_t;

/* Convenience macros */
#if defined __has_attribute
#  if __has_attribute (unused)
#    define ATTR_UNUSED __attribute__ ((unused))
#  endif
#endif
#define OPT(T) (config->T)
#define ROOT ((root_t *)root)
#define ROUND(x) ((int) ((x) + 0.5))
//typedef (square_t **) board_t;

/** Free the board array */
void
free_board(square_t ***board, int n)
{
	if(board && *board) {
		for(int i=0; i<n; i++) {
			g_free((*board)[i]);
		}
		g_free(*board);
		*board = NULL;
	}
}

/** Free the config data */
static inline void
free_config(config_t **config)
{
	if(config && *config) {
		if((*config)->sq_colour) {
			g_free((*config)->sq_colour);
		}
		if((*config)->lang) {
			g_free((*config)->lang);
		}
		if((*config)->lang_var) {
			g_free((*config)->lang_var);
		}
		if((*config)->mode) {
			g_free((*config)->mode);
		}
		g_free(*config);
		*config = NULL;
	}
}

/** Free the lexicon/alphabet data */
static inline void
free_lang(lang_t **lang)
{
	if(lang && *lang) {
		g_strfreev((*lang)->words);
		for(int i=0; i<(*lang)->l_num; i++) {
			g_free((*lang)->letters[i].label);
		}
		g_free((*lang)->letters);
	}
}

/** Callback function ran when program ends - free everything */
static inline void
shutdown(GtkApplication *app ATTR_UNUSED,
         gpointer        root)
{
	free_board(&(ROOT->board), ROOT->config->square_num);
	free_config(&(ROOT->config));
	free_lang(&(ROOT->lang));
	g_free(ROOT);
}

/** Ran when the 'quit' action is triggered, ends the main loop */
static inline void
quit_app(GSimpleAction *action ATTR_UNUSED,
         GVariant      *parameter ATTR_UNUSED,
         gpointer       app)
{
	g_application_quit(G_APPLICATION(app));
}

/** Clear/initialise the board - run before a new game starts */
static inline void
clear_board(square_t **board, int n)
{
	for(int i=0; i<n; i++) {
		for(int j=0; j<n; j++) {
			board[i][j].tile = ' ';
		}
	}
}

/** Reads a file stored in DATADIR into a string */
static inline gboolean
get_file(const char  *filename,
         char        **buf,
         gsize       *buflen)
{
	gchar *path = g_build_filename(DATADIR, filename, NULL);
	GFile *f = g_file_new_for_path(path);
	g_free(path);
	return g_file_load_contents(f, NULL, buf, buflen, NULL, NULL);
}

/** Render the tile image for the given letter */
static inline void
draw_tile(cairo_t        *cr,
          const char     *letter,
          const char     *lang,
          int             x,
          int             y,
          const config_t *config)
{
	g_assert(x <= config->square_num);
	g_assert(y <= config->square_num);

	gchar *filename = g_strconcat(letter, ".svgz", NULL);
	gchar *path = g_build_filename(DATADIR, IMG_DIR, lang, filename, NULL);
	RsvgHandle *rsvg = rsvg_handle_new_from_file(path, NULL);
	g_free(filename);
	g_free(path);

	cairo_save(cr);
	cairo_translate(cr, x*OPT(tile_width),
	                    y*OPT(tile_width));
	double scale = (double)(OPT(tile_width) - OPT(border_size)/2)/100;
	cairo_scale(cr, scale, scale);
	rsvg_handle_render_cairo(rsvg, cr);
	cairo_restore(cr);

	rsvg_handle_close(rsvg, NULL);
	g_object_unref(rsvg);
}

/** Render the text given, in the centre of the board square at the coordinates given */
static inline void
draw_text_in_square(GtkWidget                  *widget,
                    cairo_t                    *cr,
                    const char                 *text,
                    const PangoFontDescription *font,
                    int                         x,
                    int                         y,
                    const config_t             *config)
{
	PangoContext *pc = gtk_widget_get_pango_context(widget);
	PangoLayout *layout = pango_layout_new(pc);
	PangoRectangle rect;

	pango_layout_set_text(layout, text, -1);
	pango_layout_set_font_description(layout, font);
	gdk_cairo_set_source_rgba(cr, &OPT(font_colour));
	pango_layout_set_width(layout, OPT(tile_width));
	pango_layout_set_height(layout, OPT(tile_width));
	pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
	pango_layout_set_spacing(layout, 0);

	pango_layout_get_pixel_extents(layout, &rect, NULL);
	cairo_move_to(cr, (x+1)*OPT(tile_width) +  OPT(tile_width)/2,
	                  (y+1)*OPT(tile_width) + (OPT(tile_width) - rect.height)/2);
	pango_cairo_update_layout(cr, layout);
	pango_cairo_show_layout(cr, layout);

	g_object_unref(layout);
}

/** Draw elements within the board: gridlines, coloured squares, and labels */
static inline gboolean
draw_board_elements(GtkWidget *widget,
                    cairo_t   *cr,
                    gpointer   root)
{
	config_t *config = ROOT->config;

	int i, j;
	PangoFontDescription *square_font;
	PangoFontDescription *label_font;
	gchar *text[SQ_NUM];
	text[nor] = NULL;
	if(OPT(board_width) > 600) {
		text[dls] = g_strdup("DOUBLE LETTER");
		text[tls] = g_strdup("TRIPLE LETTER");
		text[dws] = g_strdup("DOUBLE WORD");
		text[tws] = g_strdup("TRIPLE WORD");
		label_font  = pango_font_description_from_string("Source Sans Pro Bold 15");
		if(OPT(board_width) > 800) {
			square_font = pango_font_description_from_string("Jost* 600 Semi 8");
		} else {
			square_font = pango_font_description_from_string("Jost* 600 Semi 6");
		}
	} else {
		text[dls] = g_strdup("DL");
		text[tls] = g_strdup("TL");
		text[dws] = g_strdup("DW");
		text[tws] = g_strdup("TW");
		label_font  = pango_font_description_from_string("Source Sans Pro Bold 12");
		square_font = pango_font_description_from_string("Jost* 600 Semi 8");
	}

	char alphabet[26][2];
	char numbers[26][3];
	for(i=0; i<26; i++) {
		alphabet[i][0] = (char)('A'+i);
		alphabet[i][1] = '\0';
		sprintf(numbers[i], "%d", i+1);
	}

	for(i=0; i<OPT(square_num); i++) {
		draw_text_in_square(widget, cr, alphabet[i], label_font, -1, i, config);
		draw_text_in_square(widget, cr, alphabet[i], label_font, OPT(square_num), i, config);
		draw_text_in_square(widget, cr, numbers[i], label_font, i, -1, config);
		draw_text_in_square(widget, cr, numbers[i], label_font, i, OPT(square_num), config);
		for(j=0; j<OPT(square_num); j++) {
			cairo_set_line_width(cr, OPT(border_size));
			cairo_rectangle(cr, (i+1)*OPT(tile_width), (j+1)*OPT(tile_width),
			                OPT(tile_width) + OPT(border_size),
			                OPT(tile_width) + OPT(border_size));
			gdk_cairo_set_source_rgba(cr, &OPT(sq_colour[ROOT->board[i][j].type]));
			cairo_fill_preserve(cr);
			gdk_cairo_set_source_rgba(cr, &OPT(border_colour));
			cairo_stroke(cr);
			if(ROOT->board[i][j].type) {
				draw_text_in_square(widget, cr, text[ROOT->board[i][j].type], square_font, i, j, config);
			}
		}
	}
	pango_font_description_free(square_font);
	pango_font_description_free(label_font);

	draw_tile(cr, "T", "English", 6, 8,  config);
	draw_tile(cr, "I", "English", 7, 8,  config);
	draw_tile(cr, "L", "English", 8, 8,  config);
	draw_tile(cr, "E", "English", 9, 8,  config);
	draw_tile(cr, "S", "English", 10, 8, config);

	for(i=1; i<SQ_NUM; i++) {
		g_free(text[i]);
	}
	return FALSE;
}

/** Calculate the dimensions of the whole board, and tiles */
static inline GtkWidget *
draw_board(root_t *root)
{
	config_t *config = root->config;

	gint sc_width, sc_height;
	gdk_window_get_geometry(gdk_screen_get_root_window(gdk_screen_get_default()),
                                                           NULL, NULL, &sc_width, &sc_height);
#ifdef MOBILE_TEST
	if(sc_width > 700) {
		sc_width = 400;
	}
	if(sc_height > 1200) {
		sc_height = 1000;
	}
#endif
	gint min_dim = MIN(sc_width, sc_height);
	if(min_dim <= 700) {
		OPT(border_size)--;
	}

	OPT(board_width) = (int)ROUND((double)min_dim  - 2*(double)min_dim*OPT(margin));
	OPT(tile_width)  = (int)ROUND((OPT(board_width) -
	                               (OPT(square_num) + 1)*OPT(border_size)
	                              )/(OPT(square_num) + 2)
	                             );

	/* Now actually draw the board based on those dimensions */
	GtkWidget *board_area = gtk_drawing_area_new();
	gtk_widget_set_size_request(board_area, OPT(board_width), OPT(board_width));
	gtk_widget_set_halign(board_area, GTK_ALIGN_CENTER);
	gtk_widget_set_valign(board_area, GTK_ALIGN_CENTER);
	g_signal_connect(G_OBJECT(board_area), "draw", G_CALLBACK(draw_board_elements), root);
	return board_area;
}

/** List available languages by searching the lexica directory */
static inline gchar **
list_langs(void)
{
	gchar *path = g_build_filename(DATADIR, LEXICA_DIR, NULL);
	GFile *f = g_file_new_for_path(path);
	g_free(path);
	GFileEnumerator *list = g_file_enumerate_children(f,
	                                                  G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
	                                                  G_FILE_QUERY_INFO_NONE,
	                                                  NULL,
	                                                  NULL);
	int i = 0;
	char **out = NULL;
	while(TRUE) {
		GFileInfo *info;
		if(!g_file_enumerator_iterate(list, &info, NULL, NULL, NULL)) {
			out = NULL;
			break;
		} else if(!info) { // End of list
			break;
		}
		const char *name = g_file_info_get_display_name(info);
		if(g_strrstr(name, LEXICA_EXT)) {
			out = g_realloc_n(out, (gsize)(i+1), sizeof(char *));
			gchar *tmp = g_strdelimit(g_strdup(name), NULL, ' ');
			gchar **parts = g_strsplit(tmp, ".", -1);
			out[i] = g_strdup(parts[0]);
			g_strfreev(parts);
			g_free(tmp);
			i++;
		}
	}
	g_file_enumerator_close(list, NULL, NULL);
	g_object_unref(list);
	g_object_unref(f);
	if(out) {
		out = g_realloc_n(out, (gsize)(i+1), sizeof(char *));
		out[i] = NULL;
	}
	return out;
}

/** List available game modes for the given language */
static inline gchar **
list_modes_for_lang(const gchar *lang)
{
	g_assert(lang);
	gchar *low_lang = g_utf8_strdown(lang, -1);
	gchar *path = g_build_filename(DATADIR, ALPHA_DIR, NULL);
	GFile *f = g_file_new_for_path(path);
	g_free(path);
	GFileEnumerator *list = g_file_enumerate_children(f,
	                                                  G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
	                                                  G_FILE_QUERY_INFO_NONE,
	                                                  NULL,
	                                                  NULL);
	int i = 0;
	char **out = NULL;
	while(TRUE) {
		GFileInfo *info;
		if(!g_file_enumerator_iterate(list, &info, NULL, NULL, NULL)) {
			out = NULL;
			break;
		} else if(!info) { // End of list
			break;
		}
		const char *name = g_file_info_get_display_name(info);
		if(g_strrstr(name, ALPHA_EXT) &&
		   g_strrstr(name, low_lang)) {
			gchar *tmp = g_strdelimit(g_strdup(name), NULL, '.');
			gchar **parts = g_strsplit(tmp, ".", -1);
			if(parts[2]) {
				out = g_realloc_n(out, (gsize)(i+1), sizeof(char *));
				out[i] = g_strdup(parts[1]);
				i++;
			}
			g_strfreev(parts);
			g_free(tmp);
		}
	}
	g_file_enumerator_close(list, NULL, NULL);
	g_object_unref(list);
	g_object_unref(f);
	g_free(low_lang);
	if(out) {
		out = g_realloc_n(out, (gsize)(i+1), sizeof(char *));
		out[i] = NULL;
	}
	return out;
}

/** Load the given lexicon into an array of strings */
static inline gchar **
load_lexicon(const gchar *lang,
             const gchar *lang_var,
                   gint  *w_num)
{
	gchar *filename;
	if(lang_var) {
		filename = g_strconcat(lang_var, "_", lang, LEXICA_EXT, NULL);
	} else {
		filename = g_strconcat(lang, LEXICA_EXT, NULL);
	}
	gchar *path = g_build_filename(DATADIR, LEXICA_DIR, filename, NULL);
	GFile *f = g_file_new_for_path(path);
	g_free(path);
	g_free(filename);
	if(!f) {
		return NULL;
	}
	GFileInputStream *base_stream = g_file_read(f, NULL, NULL);
	if(!base_stream) {
		g_object_unref(f);
		return NULL;
	}
	GDataInputStream *stream = g_data_input_stream_new(G_INPUT_STREAM(base_stream));
	gchar **out = NULL;
	gint i = 0;
	do {
		out = g_realloc_n(out, (gsize)(i+1), sizeof(char *));
		out[i] = g_data_input_stream_read_line_utf8(stream, NULL, NULL, NULL);
		i++;
	} while(out && out[i-1]);
	*w_num = i;
	g_object_unref(f);
	out = g_realloc_n(out, (gsize)(i+1), sizeof(char *));
	out[i] = NULL;
	return out;
}

/** Load the given alphabet into an array of letter_t */
static inline letter_t *
load_alphabet(const gchar *lang,
              const gchar *mode,
                    gint  *l_num)
{
	gchar *filename;
	if(!mode || !g_strcmp0(mode, "Standard")) {
		filename = g_strconcat(lang, ALPHA_EXT, NULL);
	} else {
		filename = g_strconcat(lang, "_", mode, ALPHA_EXT, NULL);
	}
	gchar *path = g_build_filename(DATADIR, ALPHA_DIR, filename, NULL);
	GFile *f = g_file_new_for_path(path);
	g_free(path);
	g_free(filename);
	if(!f) {
		return NULL;
	}
	GFileInputStream *base_stream = g_file_read(f, NULL, NULL);
	if(!base_stream) {
		g_object_unref(f);
		return NULL;
	}
	GDataInputStream *stream = g_data_input_stream_new(G_INPUT_STREAM(base_stream));
	letter_t *out = NULL;
	gint i = 0;
	do {
		gchar *line = g_data_input_stream_read_line_utf8(stream, NULL, NULL, NULL);
		// Tab separated fields are: uppercase, lowercase, score, frequency
		// We ignore lines starting with #, or with fewer than 4 fields
		if(line && line[0] != '#') {
			gchar **fields = g_strsplit(line, "\t", -1);
			if(fields[0] &&
			   fields[1] &&
			   fields[2] &&
			   fields[3]) {
				out = g_realloc_n(out, (gsize)(i+1), sizeof(letter_t));
				out[i].label = g_strdup(fields[0]);
				out[i].score = g_ascii_strtoll(fields[2], NULL, 10);
				out[i].freq  = g_ascii_strtoll(fields[3], NULL, 10);
				i++;
			}
			g_strfreev(fields);
		}
		g_free(line);
	} while(out && out[i-1].label);
	g_object_unref(f);
	*l_num = i;
	return out;
}

/** Create the initial root structure that gets passed everywhere */
static inline root_t *
setup_root(void)
{
	root_t *root = g_malloc0(sizeof(root_t));
	root->config = g_malloc0(sizeof(config_t));
	root->lang   = g_malloc0(sizeof(lang_t));

	/* Values which determine the dimensions of the board in the main window */
	root->config->square_num  = build_board_std(&(root->board)); // Number of squares in the x and y direction
	root->config->border_size = 3;                               // Thickness of border within and around the board, in pixels
	root->config->margin      = 0.05;                            // Margin around the board, as a proportion of window size

	/* Default colours used on the board */
	gdk_rgba_parse(&root->config->font_colour,    "#000000");
	gdk_rgba_parse(&root->config->border_colour,  "#000000");

	/* Default square colours - note that we assume tws is the highest premium type */
	root->config->sq_colour = g_malloc0_n(SQ_NUM, sizeof(GdkRGBA));
	gdk_rgba_parse(&root->config->sq_colour[nor], "#00786E"); //Normal squares
	gdk_rgba_parse(&root->config->sq_colour[dls], "#A2B6C9"); //Double letter score squares
	gdk_rgba_parse(&root->config->sq_colour[tls], "#157CC0"); //Triple letter score squares
	gdk_rgba_parse(&root->config->sq_colour[dws], "#DFA378"); //Double word score squares
	gdk_rgba_parse(&root->config->sq_colour[tws], "#C02321"); //Triple word score squares

	root->config->lang = g_strdup("English");
	root->config->lang_var = g_strdup("British");
	root->config->mode = g_strdup("Standard");

	root->lang->words = load_lexicon(root->config->lang,
	                                 root->config->lang_var,
	                                 &(root->lang->w_num));
	printf("%i\n", root->lang->w_num);
	gchar **tmp = list_langs();
	g_strfreev(tmp);
	return root;
}

/** A preference dialog, for the user to set the config */
static inline void
show_prefs(GSimpleAction *action    ATTR_UNUSED,
           GVariant      *parameter ATTR_UNUSED,
           gpointer       root)
{
	GtkWidget *dialog = hdy_dialog_new(GTK_WINDOW(ROOT->window));
	gtk_window_set_title(GTK_WINDOW(dialog), PROGRAM_NAME" Preferences");
	GtkWidget *content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
//	GtkWidget *scrolled_window = gtk_scrolled_window_new(NULL, NULL);
//	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
//	gtk_scrolled_window_set_propagate_natural_width(GTK_SCROLLED_WINDOW(scrolled_window), TRUE);
//	gtk_scrolled_window_set_propagate_natural_height(GTK_SCROLLED_WINDOW(scrolled_window), TRUE);

	/* Create a list box, with a Combo Box for each preference */
	config_t *config = ROOT->config;
	GtkWidget *list = gtk_list_box_new();

	/* Language choice */
	HdyComboRow *choose_lang = hdy_combo_row_new();
	GListStore *lang_list = g_list_store_new(HDY_TYPE_VALUE_OBJECT);
	HdyValueObject *obj;

	obj = hdy_value_object_new_string("Foo");
	g_list_store_insert(lang_list, 0, obj);
	g_clear_object(&obj);
	obj = hdy_value_object_new_string("Bar");
	g_list_store_insert(lang_list, 1, obj);
	g_clear_object(&obj);

	hdy_combo_row_bind_name_model(choose_lang, G_LIST_MODEL(lang_list), NULL, NULL, NULL);
	gtk_container_add(GTK_CONTAINER(list), GTK_WIDGET(choose_lang));

//	gtk_container_add(GTK_CONTAINER(list), choose_mode);
//	gtk_container_add(GTK_CONTAINER(list), choose_theme);

//	gtk_container_add(GTK_CONTAINER(scrolled_window), list);
//	gtk_container_add(GTK_CONTAINER(content), scrolled_window);
	gtk_container_add(GTK_CONTAINER(content), list);

	gtk_dialog_add_button(GTK_DIALOG(dialog), "Apply",  GTK_RESPONSE_APPLY);
	gtk_dialog_add_button(GTK_DIALOG(dialog), "Cancel", GTK_RESPONSE_CANCEL);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_APPLY);

	int result = gtk_dialog_run(GTK_DIALOG(dialog));
	switch(result)
	{
		case GTK_RESPONSE_APPLY:
			
			break;
		default:
			
			break;
	}
	gtk_widget_destroy(dialog);
}

/** A small 'About' dialog, with a description, license information and links */
static inline void
show_about(GSimpleAction *action    ATTR_UNUSED,
           GVariant      *parameter ATTR_UNUSED,
           gpointer       root)
{
	GtkWidget *dialog = hdy_dialog_new(GTK_WINDOW(ROOT->window));
	gtk_window_set_title(GTK_WINDOW(dialog), "About "PROGRAM_NAME);
	GtkWidget *content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
	GtkWidget *scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_propagate_natural_width(GTK_SCROLLED_WINDOW(scrolled_window), TRUE);
	gtk_scrolled_window_set_propagate_natural_height(GTK_SCROLLED_WINDOW(scrolled_window), TRUE);

	GtkWidget *label = gtk_label_new(NULL);
	gtk_label_set_markup(GTK_LABEL(label),
"<span font=\"Source Sans Pro Bold 30\">"PROGRAM_NAME"</span>\n\n"\
DESCRIPTION"\n"COPYRIGHT"\n\n\
This program comes with ABSOLUTELY NO WARRANTY.\n\
This is free software, and you are welcome to\n\
redistribute it under certain conditions; see the\n\
COPYING and README files for details.\n\n\
You should have received a copy of the GNU General\n\
Public License along with Tiles. If not, see\n\
<a href=\"https://www.gnu.org/licenses/\">https://www.gnu.org/licenses</a>.\n\n\
See the source code here:\n<a href=\""URL"\" title=\"Git repository\">"URL"</a>.\n\n\
For bug reports or patches, email:\n<a title=\"Email me!\" href=\"mailto:"EMAIL"\">"EMAIL"</a>.");

	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);
	gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
	gtk_container_add(GTK_CONTAINER(scrolled_window), label);
	gtk_container_add(GTK_CONTAINER(content), scrolled_window);

	gtk_dialog_add_button(GTK_DIALOG(dialog), "OK", GTK_RESPONSE_OK);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
	g_signal_connect_swapped(dialog, "response", G_CALLBACK(gtk_widget_destroy), dialog);

	gtk_widget_show_all(dialog);
}

/** Set up thw window and draw the graphics */
static inline void
setup_window(GtkApplication *app,
             gpointer        root)
{
	/* Create and configure the main window */
	GtkWidget *window = gtk_application_window_new(app);
	ROOT->window = window; // Save in root for later
	gtk_window_set_title(GTK_WINDOW(window), PROGRAM_NAME);
	gtk_window_maximize(GTK_WINDOW(window));


	GtkWidget *stack = gtk_stack_new();
	GtkWidget *sidebar = gtk_stack_sidebar_new();
	gtk_stack_sidebar_set_stack(GTK_STACK_SIDEBAR(sidebar), GTK_STACK(stack));

	GtkWidget *label = gtk_label_new("Example");
	gtk_stack_add_named(GTK_STACK(stack), label, "example");

	GtkWidget *leaflet = hdy_leaflet_new();
	gtk_container_add(GTK_CONTAINER(leaflet), sidebar);

	/* Add the header bar */
	GtkWidget *header = gtk_header_bar_new();
	gtk_header_bar_set_title(GTK_HEADER_BAR(header), "Welcome to "PROGRAM_NAME);
	gtk_header_bar_set_has_subtitle(GTK_HEADER_BAR(header), FALSE);
	gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(header), TRUE);
	gtk_window_set_titlebar(GTK_WINDOW(window), header);

	/* Create the 'Hamburger' menu button */
/*	GtkWidget *burger_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(burger_box), GTK_BUTTONBOX_EXPAND);
	GtkWidget *burger = gtk_button_new_from_icon_name("open-menu", GTK_ICON_SIZE_LARGE_TOOLBAR);
	g_signal_connect(burger, "clicked", G_CALLBACK(show_about), window);
	gtk_container_add (GTK_CONTAINER(burger_box), burger);
	gtk_header_bar_pack_start(GTK_HEADER_BAR(header), burger_box);
*/
	const GActionEntry win_entries[] =
	{
		{ .name = "preferences", .activate = show_prefs },
		{ .name = "about",       .activate = show_about }
	};
	g_action_map_add_action_entries(G_ACTION_MAP(app), win_entries, G_N_ELEMENTS(win_entries), root);

	/* Build and draw the default board */
	GtkWidget *board_area = draw_board(root);
	gtk_container_add(GTK_CONTAINER(leaflet), board_area);
	hdy_leaflet_set_visible_child(HDY_LEAFLET(leaflet), board_area);

	gtk_container_add(GTK_CONTAINER(window), leaflet);
	gtk_widget_show_all(window);
}

/** Set up the GtkApplication, create the app menu and the keyboard shortcuts */
static inline void
setup_app(GtkApplication *app,
          gpointer        root ATTR_UNUSED)
{
	g_set_application_name(PROGRAM_NAME); // Human readable name
	g_set_prgname(PROGRAM_ID);            // XXX Only necessary in GTK3

	/* Create an application menu */
	GMenu *menu = g_menu_new();
	g_menu_insert(menu, 1, "Start Game",  NULL);
	g_menu_insert(menu, 2, "Preferences", "app.preferences");
	g_menu_insert(menu, 3, "Shortcuts",   NULL);
	g_menu_insert(menu, 4, "About",       "app.about");
	g_menu_insert(menu, 5, "Quit",        "app.quit");
	gtk_application_set_app_menu(GTK_APPLICATION(app), G_MENU_MODEL(menu));

	/* Set some common keyboard shortcuts */
	const GActionEntry app_entries[] =
	{
		{ .name = "quit", .activate = quit_app }
	};
	g_action_map_add_action_entries(G_ACTION_MAP(app), app_entries, G_N_ELEMENTS(app_entries), app);
	const gchar *quit_accels[2] = { "<Ctrl>q", NULL };
	gtk_application_set_accels_for_action(GTK_APPLICATION(app), "app.quit", quit_accels);
}

/** Main entry point: initialise libhandy, declare the GtkApplication, attach the callbacks and run the main loop */
int
main(int    argc,
     char **argv)
{
	/* Initialise and handle options */
/*	GError *err = NULL;
	gboolean showver = FALSE;
	const gchar *desc = "\n  "DESCRIPTION;
	GOptionEntry entries[] =
	{
	  { "version", 'v', 0, G_OPTION_ARG_NONE, &showver, "Show version and license information", NULL },
	  { NULL }
	};
	gboolean success = gtk_init_with_args(&argc, &argv, desc, entries,  NULL, &err);
	if(!success) {
		fprintf(stderr, "%s: Failed to initialise GTK - %s\n", argv[0], err->message);
		g_error_free(err);
		return EXIT_FAILURE;
	} else if(showver) {
		printf("%s %s\n", argv[0], VERSION);
		printf("%s\n", COPYRIGHT);
		printf("This program comes with ABSOLUTELY NO WARRANTY.\n");
		printf("This is free software, and you are welcome to redistribute it under certain conditions;\n");
		printf("see the COPYING file or %s for details.\n", URL);
		return EXIT_SUCCESS;
	}
*/
	/* Try to initialise libhandy             */
	if(!hdy_init(&argc, &argv)) {
		fprintf(stderr, "%s: could not initialise libhandy. Aborting.", argv[0]);
		return EXIT_FAILURE;
	}

	/* Initialise root (default config/board) */
	root_t *root = setup_root();

	/* Create the application                 */
	GtkApplication *app = gtk_application_new(PROGRAM_ID, G_APPLICATION_FLAGS_NONE);

	/* Connect the callback functions         */
	g_signal_connect(app, "startup",  G_CALLBACK(setup_app),    root);
	g_signal_connect(app, "activate", G_CALLBACK(setup_window), root);
	g_signal_connect(app, "shutdown", G_CALLBACK(shutdown),     root);

	/* Run the main loop                      */
	int status = g_application_run(G_APPLICATION(app), argc, argv);

	/* Tidy up                                */
	g_object_unref(app);
	return status;
}
