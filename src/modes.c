/**
@file
@author Copyright (C) 2019 Oliver Galvin

@brief
  Allocating and setting up the board array, with a function for each game mode
  which is run on startup of when the board is reconfigured.

@section LICENSE
  This file is part of Tiles, a GTK-based crossword game.

  Tiles is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Tiles is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Tiles.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "modes.h"

#define BOARD (*board)
#define REFLECT(x, y, T) BOARD[x][y].type = T; BOARD[y][x].type = T

/** Allocate memory to, and initialise, the board */
static inline square_t **
alloc_board(int n)
{
	square_t **board = g_malloc_n((gsize)n, sizeof(square_t *));
	for(int i=0; i<n; i++) {
		board[i] = g_malloc_n((gsize)n, sizeof(square_t));
		for(int j=0; j<n; j++) {
			board[i][j].tile = ' ';
			board[i][j].type = nor;
		}
	}
	return board;
}

/** Set up a standard scrabble-style board arrangement */
int
build_board_std(square_t ***board)
{
	gint max = 14;
	free_board(board, max+1);
	BOARD = alloc_board(max+1);
	for(int i=0; i<=max; i++) {
		BOARD[i][i].type = dws;               // 17 DWs
		BOARD[i][max-i].type = dws;
		if(i==0 || i==max) {                  // 8 TWs
			BOARD[i][i].type = tws;
			REFLECT(i, max-i, tws);
			REFLECT(i, max/2, tws);
		} else if(i==3 || i==max-3) {         // 12 DLs
			REFLECT(0, i, dls);
			REFLECT(i, max, dls);
			REFLECT(i, max/2, dls);
		} else if(i==max/2-1 || i==max/2+1) { // 12 DLs
			BOARD[i][i].type = dls;
			REFLECT(i, max-i, dls);
			REFLECT(i, max-2, dls);
			REFLECT(i, 2, dls);
		} else if(i==max/2-2 || i==max/2+2) { // 12 TLs
			BOARD[i][i].type = tls;
			REFLECT(i, max-i, tls);
			REFLECT(i, max-1, tls);
			REFLECT(i, 1, tls);
		}
	}
	return max+1;
}
