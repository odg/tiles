/**
@file
@author Copyright (C) 2019 Oliver Galvin

@brief

@section LICENSE
  This file is part of Tiles, a GTK-based crossword game.

  Tiles is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Tiles is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Tiles.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MODES_H
#define MODES_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

/** Types of premium squares */
typedef enum premium_t
{
        nor = 0, ///< Normal
        dls = 1, ///< Double letter
        tls = 2, ///< Triple letter
        dws = 3, ///< Double word
        tws = 4  ///< Triple word
} premium_t;

/** Number of square types */
#define SQ_NUM 5

/** Content and type of square */
typedef struct square_t
{
        gchar tile;     ///< Tile in the square, if any
        premium_t type; ///< Type of square, if it is premium
} square_t;

void free_board(square_t ***board, int n);

int build_board_std(square_t ***board);

G_END_DECLS

#endif /* MODES_H */
