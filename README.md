# Tiles #

Tiles is a crossword game, which has various modes with rules based on popular
games such as Scrabble, Words With Friends, and more. It has a modern,
mobile-friendly graphical interface based on GTK and libhandy.

## Building

To build, you need:
* C99 compiler (i.e. gcc, clang)
* meson
* ninja
* pkg-config
* GTK 3 or later
* librsvg

then do:

    meson . _build
    ninja -C _build
    ninja -C _build install

Meson supports backends other than Ninja too, like msbuild and xcode.  
There is a legacy makefile, but it's not recommended as its less tested, a
handwritten makefile is more fragile, and Meson integrates better with libhandy
and GNOME.

## License

Tiles is Copyright (C) Oliver Galvin 2019, released under the terms of the GNU
General Public License, version 3 or, at your option, any later version, as
published by the Free Software Foundation.

See the `COPYING` file for further information.

See the `data/THANKS.md` file for credits for the various files in distributed
with Tiles.
