Welcome to Tiles' documentation!
================================

.. image:: ../data/images/icon.svg
   :width: 200px
   :align: center

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   guide
   code/main
   code/modes
   credits
   about

Tiles is a responsive app, written in C using GTK, which is a crossword game,
inspired by the likes of Scrabble and Words With Friends.

This documentation aims to offer both a brief user guide, and a developer guide
documenting the code.

Indices 
-------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
