#!/usr/bin/python3
"""A small Python script to generate sets of SVG tile images, for a given alphabet.
Part of Tiles, a GTK-based crossword game, inspired by games like Scrabble."""
# Copyright (C) 2019 Oliver Galvin
#
#  Tiles is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Tiles is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Tiles.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys

__copyright__ = "Copyright 2019 Oliver Galvin"
__license__ = "GPL3+"

list = []

def eprint(*args, **kwargs):
    """Print to stderr"""
    print(*((sys.argv[0], ":") + args), file=sys.stderr, **kwargs)

def download_file(url):
    """Download an arbitrary file at 'url'"""
    import requests
    import shutil
    filename = url.split('/')[-1]
    if os.path.isfile(filename):
        print("File already exists: " + filename)
        return filename
    print("Downloading: " + filename + "...")
    with requests.get(url, stream=True) as r:
        with open(filename, 'wb') as f:
            shutil.copyfileobj(r.raw, f)
    print("done.")
    return filename

def word_cb(data):
    """Callback function to check and add a word to the dictionary"""
    word = data["word"].upper()
    pos = data["pos"]
    length = len(word)
    if length > 1 and length <= 26 and word.isalpha() and pos not in ["name", "punct", "suffix", "prefix", "digit", "symbol", "proverb", "phrase", "abbrev", "clitic"]:
        list.append(word)

def gen_dict(lang, outdir):
    """
    Generates a dictionary for the given language.
    Downloads the relevant Wiktionary dump if necessary,
    and parses it into a simple word list file.

    'lang' is the language's full name, i.e. "English" or "French"
    """
    import pycountry
    import wiktextract
    short_lang = pycountry.languages.get(name=lang).alpha_2
    url = "https://dumps.wikimedia.org/" + short_lang + "wiktionary/latest/" + short_lang + "wiktionary-latest-pages-articles.xml.bz2"
    filename = download_file(url)
    output = os.path.join(outdir, lang, "dict.txt")
    list = []
    print("Extracting data from Wiktionary dump...")
    ctx = wiktextract.parse_wiktionary(
        filename, word_cb,
        capture_cb=None,
        languages=[lang],
        translations=False,
        pronunciations=False,
        redirects=False)
    print("Sorting word list...")
    list.sort()
    print("Saving list to file...")
    with open(output, 'wb') as f:
        for w in list:
            f.write("%s\n" % w)

def gen_tiles(alphabets, template, outdir, gendict):
    """
    Generates an SVG for each letter in each given alphabet.

    'alphabets' is a list of .quackle_alphabet filenames
    'template'  is the name of an SVG file to use as a template
    'outdir'    is the directory to write files to
    """
    # TODO:
    # Adjust font in Mandarin, Korean, Pinyin and Russian
    # Korean bars?
    import csv
    from lxml import etree
    try:
        svg = etree.parse(template)
    except (FileNotFoundError, OSError):
        eprint("failed to open template file - " + template)
        exit(1)
    except etree.XMLSyntaxError:
        eprint("invalid XML file - " + template)
        exit(1)
    svg_root = svg.getroot()
    ns = "{http://www.w3.org/2000/svg}"
    t = dict([(x.get("id"), x[0]) for x in svg_root.iterfind(".//" + ns + "text[@id]")])
    svg_root.remove(svg_root.find(ns + "metadata"))
    title = svg_root.find(ns + "title").text
    score_x = float(t["score"].get("x"))
    ext = ".quackle_alphabet"

    print("Using the template file '" + template + "'")
    print("Saving output in '" + outdir + "'")
    try:
        os.mkdir(outdir)
    except FileExistsError:
        pass
    for alphabet in alphabets:
        if not alphabet.endswith(ext):
            eprint(alphabet + " does not appear to be a Quackle Alphabet file")
            continue
        try:
            with open(alphabet, "r", encoding="utf-8") as tsvin:
                print("Generating tile images for " + alphabet)
                lang = os.path.basename(alphabet).replace(ext, "").replace("_", " ").title()
                tsv = csv.reader(tsvin, delimiter="\t")
                for row in tsv:
                    if row[0].startswith("#") or "blank" in row[0] or len(row) < 3:
                        continue
                    t["letter"].text = row[0].replace("|", "")
                    t["score"].text = row[2]
                    if int(t["score"].text) >= 10:
                        t["score"].set("x", str(score_x - 2))
                    else:
                        t["score"].set("x", str(score_x))
                    svg_root.find(ns + "title").text = "%s - %s - %s" % \
                                                       (title, t["letter"].text, lang)
                    try:
                        os.mkdir(os.path.join(outdir, lang))
                    except FileExistsError:
                        pass
                    output = os.path.join(outdir, lang, t["letter"].text + ".svgz")
                    svg.write(output, encoding="utf-8", compression=9)
            if gendict:
                print("Generating " + lang + " dictionary")
                gen_dict(lang, outdir)
        except FileNotFoundError:
            eprint("file not found - " + alphabet)
            continue
    # Now create a blank tile, which is not language-specific
    t["letter"].text = ""
    t["score"].text = ""
    svg_root.find(ns + "title").text = title +  " - Blank"
    output = os.path.join(outdir, "blank.svgz")
    svg.write(output, method="c14n", with_comments=False, compression=9)


if __name__ == "__main__":
    import argparse
    P = argparse.ArgumentParser(description=__doc__)
    P.add_argument("-t", "--template", metavar="FILE", default="images/tile.svg",
                   help="the template SVG file to use")
    P.add_argument("-o", "--output", metavar="DIR", default="images",
                   help="the output directory to use (default: tiles)")
    P.add_argument("-d", "--dictionary", action="store_true",
                   help="whether to also download and generate dictionaries (warning: slow)")
    P.add_argument("alphabet", nargs="+",
                   help="the .quackle_alphabet file(s) to base the tile images on")
    A = P.parse_args()
    gen_tiles(A.alphabet, A.template, A.output, A.dictionary)
