# Data

This directory contains data to be installed with Tiles.
It should be installed in `$PREFIX/share/Tiles`, alongside the README and license.

## Origin

This is a list of the non-code files distributed with Tiles, along with their
description, author, license, how they have been generated/modified, and a URL
if applicable. The list is sorted by the subdirectory the files are in.

* /data/fonts
  * Jost* - by Owen Earl, using the 'SIL Open Font License', see <https://github.com/indestructible-type/Jost>
  * Source Sans Pro - by Paul D. Hunt for Adobe, using the 'SIL Open Font License', see <https://github.com/adobe-fonts/source-sans-pro>

* /data/images
  * tile.svg - by Oliver Galvin, using the same license as the rest of Tiles
  * icon.svg - based on tile.svg
  * All *.svgz images are based on tile.svg and the alphabet files, and generated with gen_tiles.py

* /data/lexica - word lists in each language - generated using gen_dicts.sh
  * polish - <https://sjp.pl/slownik/growy> - GPL 2/CC-BY 4 licensed
  * turkish - from Quackle, based on the Zemberek project
  * norwegian - from Quackle, based on data here: <http://www2.scrabbleforbundet.no/?attachment_id=1620>
  * english_{british,american,canadian} - generated from the SCOWL large word lists
  * french, swedish - generated from /usr/share/dict lists in Debian

* /data/alphabets - storing the score and distribution of letters in each language
  * all files ending in .quackle_alphabet come from the Quackle project, licensed GPLv3+

All source files, and any other files not listed here, are Copyright Oliver
Galvin, use the same license as the rest of Tiles, GPLv3+. If you have any
questions or notice any mistakes or omissions, email odg@riseup.net.

## Copyright

libhandy is released by Purism under the GNU Lesser General Public License 2.1.

libgtk, glib, librsvg are part of the GNOME project, also using the GNU LGPL.

Files from the Quackle project are Copyright (C) 2005-2019 Jason Katz-Brown,
John O'Laughlin, and John Fultz.

From the README included with the Polish word list:

    Słownik SJP.PL - wersja do gier słownych
    Słownik udostępniany na licencjach GPL 2 oraz
    Creative Commons Attribution 4.0 International
    https://sjp.pl

