#!/bin/sh

# Part of Tiles, a GTK-based crossword game, inspired by games like Scrabble.
# Copyright (C) 2019 Oliver Galvin
#
#  Tiles is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Tiles is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Tiles.  If not, see <https://www.gnu.org/licenses/>.
#
# To run this script you will need a POSIX shell, grep, GNU wget and the necessary dictionary files

dir="lexica"
prefix="/usr"
ext=".list"
dicts="${prefix}/share/dict"

mkdir -p "$dir"

echo "Saving word lists in $(pwd)/${dir}"
echo "Generating lists from data in ${dicts}..."
LANG=en grep -E "^[[:lower:]]{,26}$" "${dicts}/british-english-large" | sed -e 's/\(.*\)/\U\1/' > "${dir}/British_English${ext}"
LANG=en grep -E "^[[:lower:]]{,26}$" "${dicts}/american-english-large" | sed -e 's/\(.*\)/\U\1/' > "${dir}/American_English${ext}"
LANG=en grep -E "^[[:lower:]]{,26}$" "${dicts}/canadian-english-large" | sed -e 's/\(.*\)/\U\1/' > "${dir}/Canadian_English${ext}"
LANG=fr grep -E "^[[:lower:]]{,26}$" "${dicts}/french" | sed -e 's/\(.*\)/\U\1/' > "${dir}/French${ext}"
LANG=sv grep -E "^[[:lower:]]{,26}$" "${dicts}/swedish" | sed -e 's/\(.*\)/\U\1/' > "${dir}/Swedish${ext}"

#LANG=fr grep -E "^[[:lower:]]{,26}$" "${dicts}/greek" > "${dir}/greek"
#LANG=fr grep -E "^[[:lower:]]{,26}$" "${dicts}/korean" > "${dir}/korean"
#LANG=fr grep -E "^[[:lower:]]{,26}$" "${dicts}/mandarin" > "${dir}/mandarin"
#LANG=fr grep -E "^[[:lower:]]{,26}$" "${dicts}/pinyin" > "${dir}/pinyin"
#LANG=fr grep -E "^[[:lower:]]{,26}$" "${dicts}/russian" > "${dir}/russian"
#LANG=fr grep -E "^[[:lower:]]{,26}$" "${dicts}/tuvan" > "${dir}/tuvan"
#LANG=fr grep -E "^[[:lower:]]{,26}$" "${dicts}/zhuyin" > "${dir}/zhuyin"

echo "Downloading free lists from the Quackle project..."
url="https://raw.githubusercontent.com/quackle/quackle/master/data/raw_lexica"
wget -nc -q "${url}/turkish.raw" -O - | grep -E "^.{,26}$" > "${dir}/Turkish${ext}"
wget -nc -q "${url}/norwegian.raw" -O - | grep -E "^.{,26}$" > "${dir}/Norwegian${ext}"

#In Debian:
#brazilian
#bulgarian
#catalan
#danish
#dutch
#italian
#german
#portuguese
#spanish
